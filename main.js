(function()
{
    const title = document.getElementById("title");
    const message1 = "Basic Progressive Web Application";
    const message2 = "Nothing Else To See Here";
    const serviceWorkerIsSupported = "serviceWorker" in navigator;
    
    if (serviceWorkerIsSupported)
    {
        navigator.serviceWorker.register("service-worker.js")
            .then(reg => console.log("Service worker registered", reg))
            .catch(err => console.log("Service worker not registered", err));
    }
    else
    {
        console.log("Service worker not supported");
    }


    startChangingTitle();


    async function startChangingTitle()
    {
        while (true)
        {
            await setTitle(message1, 15);
            await sleep(3000);
            await setTitle(message2, 15);
            await sleep(3000);
        }
    }

    async function setTitle(text, cooldownBetweenSymbols)
    {
        for (let i = 1; i <= text.length; i++)
        {
            title.textContent = text.substr(0, i);
            await sleep(cooldownBetweenSymbols);
        }
    }

    function sleep(ms)
    {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

})();
