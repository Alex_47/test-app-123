const staticCacheName = "site-static";
const assets = [
    "index.html",
    "main.js",
    "style.css",
    "assets/icon_192x192.png",
    "assets/icon_512x512.png"
];

self.addEventListener("install", event =>
{
    // console.log('Service worker has been installed');
    
    event.waitUntil(
        caches
            .open(staticCacheName)
            .then(cache =>
            {
                console.log("Caching shell assets");
                cache.addAll(assets);
            })
    );
});

self.addEventListener("activate", event =>
{
    // console.log('Service worker has been activated');

});

self.addEventListener("fetch", event =>
{
    // console.log('Service worked has fetched something', event.request);

    // If the desired file is in the cache, grab the one there
    // otherwise just fetch things normally from the server
    event.respondWith(caches
        .match(event.request)
        .then(response => response || fetch(event.request)));
});


// window.addEventListener('beforeinstallprompt', e =>
// {
//     console.log('beforeinstallprompt Event fired');
//     e.preventDefault();
//     // Stash the event so it can be triggered later.
//     this.deferredPrompt = e;
//     return false;
// });

// // When you want to trigger prompt:
// this.deferredPrompt.prompt();
// this.deferredPrompt.userChoice.then(choice => console.log(choice));
// this.deferredPrompt = null;

